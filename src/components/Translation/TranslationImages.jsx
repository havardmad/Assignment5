


function TranslationImages({word}){
    const images = []
    const regex = /[a-z]/
    for(let i in word){
        if(regex.test(word[i]) === true){
            images.push(<img src ={"/signs/"+word[i] +".png"}/>)  
        }
        else if(word[i] === ' '){
            images.push(<img src="/signs/space.png"></img>)
        }
        else{
            console.log("Cannot print that special character")
        }
        
    }

      return(
        <>
            {images}
        </>
    )
}  
export default TranslationImages


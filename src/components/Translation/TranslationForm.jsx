import {useState} from 'react'
import { addTranslation } from '../../api/translation'
import { useUser } from '../../contexts/UserProvider'
import { storageSave } from '../../utils/Storage'
import TranslationImages from './TranslationImages'


function TranslationForm(){

    const [translate,setString] = useState({
        value: ''
    }
    )
    const { user , setUser } = useUser()

    const onSubmit = async event => {
        event.preventDefault()
        
        TranslationImages(translate.value)
        console.log("USER:", user)
        const [error,result] = await addTranslation(translate.value, user)
        console.log("Result: "+ result.translations)
        console.log("Error: ", error)
        storageSave('user', result)
        setUser(result)
    }
    const handleChange = event => {
        event.preventDefault()
        setString({value: event.target.value})    
    }

  
 
    return(
        <>
        <h2>Please enter what you want to translate</h2>
        <div className='TranslationForm'>
            <form onSubmit={onSubmit}>
                <input type="textbox"
                placeholder='Sentence'
                onChange={handleChange}/>
                
                 
            
            <button type="submit">Save</button>
            </form>
        </div>
        <div className='ImageView'>
            <TranslationImages word={translate.value}/>
        </div>
        
        </>
    )

}
export default TranslationForm
import {  useState } from "react"
import { updateUser, updateUsername } from "../../api/user"
import React from "react"
import { useUser } from "../../contexts/UserProvider"
import { removeTranslations } from "../../api/translation"
import { storageSave } from "../../utils/Storage"

function UserInformation(){
    const {user, setUser} = useUser()
    
    
    
    const[username,setUsername] = useState({
        value: user.username
    })
    

    let translationList =user.translations.slice(-10).map((item,index)=>{
        return <li key={index}>{item}</li>
    })
    
 
    

    const onSubmit = async event => {
        event.preventDefault()
        const[error,data] = await updateUsername(username.value , user.id)
        setUser(data)
        console.log(data)
            
    }
    const onDelete = async event => {
        event.preventDefault()
        
        const[error,result] = await removeTranslations(user.id)
        if(error !== null){
            console.log(error)
        }
        else{
            storageSave(result)
            setUser(result)
        }
    }

    const handleChange = event => {
        setUsername({
            ...username,
            value: event.target.value
          
        })
        console.log(username.value)
      }
    
    return(
        <>
        <div className="EditUser">
        <form>
            <h2>User information</h2>
            <label htmlFor="username">Edit Username:</label>
            <input type = "text" value={username.value} onChange={handleChange}/>
            
                <h3>Translations:</h3>
            <ul>
               {translationList}
            </ul>
            <div className="UserProfileButtons">
                <button type = "button"className="btn btn-danger" onClick={onDelete}>Delete translation history</button>
                <button type = "submit" className="btn btn-primary" onClick={onSubmit}>Save changes</button> 
            </div>
            
            
            
        </form>
        </div>
        </>
    )
}
export default UserInformation
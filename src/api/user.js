import { createHeaders } from "."
const apiURL = process.env.REACT_APP_API_URL



export async function checkForUser(username) {
    try{
    const response = await fetch(`${apiURL}/translations?username=${username}`,{
    headers : createHeaders()
    })
    const data = await response.json()
    return [null , data.pop()]
    }
    
    catch(error) {
        
        return [error.message,null]
    }
}

async function createUser(username){
  try{
    const response = await fetch(`${apiURL}/translations`, {
    method: 'POST',
    headers: createHeaders(),
    body: JSON.stringify({ 
        username: username, 
        translations: [] 
    })
  })
  if(!response.ok){
    throw new Error("Could not create user: " + username)
  }
  const data = await response.json()
  return [null, data]
  }
  catch(error){
    return [error.message, []]
  }
  

}

export async function loginUser(username){
    const [error,user] = await checkForUser(username)
    if(error !== null){
      return [error.message,null]
    }
    if(user){
      return [null,user]
    }
      
      
    return await createUser(username)
    
}


export async function updateUsername(update,userId){
  try{
    console.log(JSON.stringify(update))
    const response = await fetch(`${apiURL}/translations/${userId}`, {
        method: 'PATCH', 
        headers: createHeaders(),
        body: JSON.stringify({
          username: update 
        })
    })
    const data = await response.json()
    return [null,data]
  }
  catch(error){
    return [error.message, null]
  }
  
}


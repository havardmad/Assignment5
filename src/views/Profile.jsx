import React, { useEffect } from 'react'
import  UserInformation  from '../components/Profile/UserInformation'
import withAuth from '../hoc/withAuth'
import { useUser } from '../contexts/UserProvider'
import UserHeader from '../components/Profile/UserHeader'

function Profile(){
    
    const { user } = useUser()
    

    
    return(
        <>
        <UserHeader username = {user.username}/>
        <UserInformation user={user}/>
        </>
        
    )   
}
export default withAuth(Profile)
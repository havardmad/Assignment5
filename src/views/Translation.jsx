import TranslationForm from "../components/Translation/TranslationForm"
import TranslationImages from "../components/Translation/TranslationImages"
import withAuth from "../hoc/withAuth"

function Translation(){
    
    return(
        <div>
            <TranslationForm/>
            <TranslationImages></TranslationImages>
        </div>
        
    )
}
export default withAuth(Translation)